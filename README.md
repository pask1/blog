`yarn start` to start dev server on port `1234`

`yarn build` to build for production (not tested/needs work)

## TODO
- [ ] Write proper description in info section
- [ ] Set up links in info section
- [ ] Add 404/not found route + page
- [ ] Create components for article/project lists
- [ ] Create content pages for articles/projects (possibly parse in markdown files?)
- [ ] Add section/link for CV/portfolio
- [ ] Add light/dark/fancy theme switcher
- [ ] Clean up development junk
  - [ ] Subcomponents should (for the most part) not need `import React, { useCallback (...)` - remove this
  - [ ] Clean up SCSS (split into separate files?
  - [ ] Remove nonfunctional and debug code
  - [ ] Reformat some sections
  - [ ] Clean up and/or update `tsconfig.json`
  - [ ] Clean up dependencies
- [ ] Move `app.(tsx|scss)` back to root of `components` and update imports accordingly
- [ ] Fix production part of `esbuild.config.js` and test this
- [ ] Add meta tags
