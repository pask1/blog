import React, { useCallback, useState } from 'react';
import ReactDOM from 'react-dom';
import DocumentMeta from 'react-document-meta';
import { createRoot } from 'react-dom/client';
import { createBrowserRouter, RouterProvider } from "react-router-dom";

// Components
import Main from '../main/main.tsx';
import Articles from '../content/articles.tsx';
import Projects from '../content/projects.tsx';
import Contact from '../content/contact.tsx';

import './app.scss';

const router = createBrowserRouter([
    {
        path: "/",
        element: <Main/>,
        children: [
            { index: true, element: <Articles/> },
            {
                path: "projects",
                element: <Projects/>,
            },
            {
                path: "contact",
                element: <Contact/>,
            },
        ],
    },
]);

// const App = (props: {message: string }) => {

//     const meta = {
//       title: 'Some Meta Title',
//       description: 'I am a description, and I can create multiple tags',
//       canonical: 'http://example.com/path/to/page',
//       meta: {
//         charset: 'utf-8',
//         name: {
//           keywords: 'react,meta,document,html,tags'
//         }
//       }
//     };

//     return (<>
//         <Main/>
//     </>);
// };

createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
);
