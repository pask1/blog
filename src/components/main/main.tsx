import React, { useCallback, useState } from 'react';
import { Outlet, Link, NavLink } from "react-router-dom";

const Main = () => {
    return (
        <div className="main">
            <div className="mainSection-wrapper">
                <div className="mainSection">
                    <div className="sidebar">

                        <a className="nameLink" href="/">
                            <img className="avatar" src="avatar.jpg" alt="Avatar" width="60" height="60"  />
                            <div className="name">Nils Korsfeldt</div>
                        </a>
                        <div className="description">some description here asdf asdölkjf lorem ipsum dolor sit amet and so on and so forth and so forth and so on yaknowwhatimean?</div>

                        <div className="links">
                            <span><a href="">cgit</a></span>
                            <span><a href="">dots</a></span>
                            <span><a href="">Insta?</a></span>
                            <span><a href="">YouTube</a></span>
                            <span><a href="">asdf</a></span>
                            <span><a href="">@</a></span>
                        </div>

                        <div className="nav">
                            <ul className="nav-ul">
                                <li><NavLink to='/'>Articles</NavLink></li>
                                <li><NavLink to='/projects'>Projects</NavLink></li>
                                <li><NavLink to='/contact'>Contact</NavLink></li>
                            </ul>
                        </div>

                    </div>
                    <div className="content">
                        <h1>Content div</h1>
                        <Outlet/>
                    </div>
                </div>
            </div>
            <div className="footer">
                <p>This is a footer</p>
            </div>
        </div>
    );
};

export default Main;
