import * as esbuild from 'esbuild';
import http from 'node:http';
import {sassPlugin} from 'esbuild-sass-plugin';

// Function to serve SPA properly
// Credit to: https://gist.github.com/martinrue/2896becdb8a5ed81761e11ff2ea5898e
const serve = async (context, servedir, listen) => {
  // Start esbuild's local web server. Random port will be chosen by esbuild.
  const { host, port } = await context.serve({ servedir }, {});

  // Create a second (proxy) server that will forward requests to esbuild.
  const proxy = http.createServer((req, res) => {
    // forwardRequest forwards an http request through to esbuid.
    const forwardRequest = (path) => {
      const options = {
        hostname: host,
        port,
        path,
        method: req.method,
        headers: req.headers,
      };

      const proxyReq = http.request(options, (proxyRes) => {
        if (proxyRes.statusCode === 404) {
          // If esbuild 404s the request, assume it's a route needing to
          // be handled by the JS bundle, so forward a second attempt to `/`.
          return forwardRequest("/");
        }

        // Otherwise esbuild handled it like a champ, so proxy the response back.
        res.writeHead(proxyRes.statusCode, proxyRes.headers);
        proxyRes.pipe(res, { end: true });
      });

      req.pipe(proxyReq, { end: true });
    };

    // When we're called pass the request right through to esbuild.
    forwardRequest(req.url);
  });

  // Start our proxy server at the specified `listen` port.
  proxy.listen(listen);
};

if (process.env.NODE_ENV = "dev") {

    const watchRebuild = {
        name: 'watchRebuild',
        setup(build) {
        let count = 0;
        build.onEnd(result => {
            if (count++ === 0) console.log('first build:', result);
            else console.log('subsequent build:', count, result);
            });
        },
    };

    let ctx = await esbuild.context({
        entryPoints: ['src/components/app/app.tsx'],
        outfile: 'public/bundle.js',
        bundle: true,
        minify: false,
        // watch: true,
        // watch: {
        //     onRebuild(error, result) {
        //         console.log(error ? error : result)
        //     }
        // },
        plugins: [watchRebuild, sassPlugin()],
    })

    await ctx.watch();
    
    // Serves all content from ./public on :1234.
    // If esbuild 404s the request, the request is attempted again
    // from `/` assuming that it's an SPA route needing to be handled by the root bundle.
    serve(ctx, "public", 1234);

} else {
    esbuild.build({
        entryPoints: ['src/app.tsx'],
        outfile: 'public/bundle.js',
        bundle: true,
        minify: true,
        plugins: [sassPlugin()]
    }).then(result => {
        console.log(result);
        console.log("Built for prod");
    });
}
